$(document).ready(function(){
  var windowHeight = window.innerHeight;
  var $header = $('.header');
  $header.css({'min-height': windowHeight +'px'});
  $header.addClass('showAnim');

  $('.testimonials').slick({
    centerPadding: '60px',
    slidesToShow: 2,
    cssEase: 'ease-out',
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 1,
          variableWidth: false
        }
      }
    ]
  });

  $('.screens').slick({
    infinite: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: 'ease-out',
    fade: true
  });

  var $buttonScrollTo = $('.scrollTo');
  var $form = $('.section--contact');
  $buttonScrollTo.click(function(e){
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $form.offset().top
    }, 1000);
  });

  var $headerSlider = $(".header__slider");
  function onScroll() {
    $headerSlider.css({'margin-top': '-'+ window.scrollY * 0.3 +'px'});
  }
  window.addEventListener('scroll', onScroll);

  $("textarea").keyup(function(e) {
    $(this).height(1);
    $(this).height(this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth")) - 21);
  });

  setTimeout(function() {
    $('#contactForm-custom-user-id').val(ga.getAll()[0].get('clientId'));
  }, 2000);
  $('#contactForm-auto-referrer').val(document.location.href);
  $.getJSON('https://freegeoip.net/json/', function(data){
  $("#contactForm-country").val(data.country_name);
    $('#contactForm-phone').intlTelInput({
      initialCountry: data.country_code,
      nationalMode: false,
      preferredCountries: [],
    });
  });
});

function getCoords(elem) {
  var box = elem.getBoundingClientRect();

  var body = document.body;
  var docElem = document.documentElement;

  var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

  var clientTop = docElem.clientTop || body.clientTop || 0;
  var clientLeft = docElem.clientLeft || body.clientLeft || 0;

  var top  = box.top +  scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;

  return { top: Math.round(top), left: Math.round(left) };
}
/**
 * Перевіряємо елемент чи попадає у видиму частину екрану
 * Для попадяння достатньо, щоб верхня або нижня границя елемента була видна.
 */
function isVisible(elem) {

  var coords = getCoords(elem);
  var windowTop = window.pageYOffset || document.documentElement.scrollTop;
  var windowBottom = windowTop + document.documentElement.clientHeight;

  coords.bottom = coords.top + elem.offsetHeight;

  // Верхея границя elem попала у виидиму частину АБО нижня границя видима
  var topVisible = coords.top > windowTop && coords.top < windowBottom;
  var bottomVisible = coords.bottom < windowBottom && coords.bottom > windowTop;

  return topVisible || bottomVisible;
}
function showVisible() {
  var divs = document.getElementsByTagName('section');
  for(var b=0; b < divs.length; b++) {
    var div = divs[b];
    var title = div.getAttribute('title');
    if (!title) continue;
    if (isVisible(div)) {
      div.className = div.className + ' ' + title;
      div.setAttribute('title', '');
    }
  }

}

window.onload = showVisible;
window.onscroll = showVisible;
showVisible();

function sendContactForm() {
  $(".contactForm .btn").addClass("submit");
  var form = $('#contactForm');
  var formData = new FormData(form[0]);
  $.ajax({
    url: '/contact-form-submit',
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    success: function(data) {
      if(data != '') {
        alert(data + '. Try again.');
        return;
      }
      ga('send', 'event', 'RetailAppsAssociatesForm', 'Contact form submitted', window.location.href);
      window.location.href = "/retail-thank-you";
    },
    error: function() {
      alert('An error occurred. Try again later.');
    },
    complete: function() {
      grecaptcha.reset();
      $(".contactForm .btn").removeClass("submit");
    }
  });
};

function validateContactForm(event) {
  event.preventDefault();
  var form = $('#contactForm');

  var isValid = isValidControl(form.find('[name="Name"]'), function (name) { return name.val() != ''; });
  isValid &= isValidControl(form.find('[name="Email"]'), function (email) { return validateEmail(email.val()); });
  isValid &= isValidControl(form.find('[name="Message"]'), function (message) { return message.val() != ''; });

  if(isValid) {
    grecaptcha.execute();
  }
}

function isValidControl(control, predicate) {
  if(predicate(control)) { control.removeClass('error'); return true; }
  else { control.addClass('error'); return false; }
}

function validateEmail(email) {
  var r = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return r.test(email);
}